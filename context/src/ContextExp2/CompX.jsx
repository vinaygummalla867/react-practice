import React from 'react';
import CompE from '../ContextExp/CompE';
import { appContext } from './CompU';
import { useContext } from 'react';


const CompX = ({age})=>{
    let data = useContext(appContext);
    return <> 
                       <h3>{`CompX with context data. Age:${data}`}</h3>
                
    <h3>CompX Props data. Age:{age}</h3>
    <CompE />
    </>
       
    
}

export default CompX;