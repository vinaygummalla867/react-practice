import React from 'react';
import CompW from './CompW';
import CompZ from './CompZ';


const CompV = ({age})=>{
    return <>
     <CompW age={age} />
     <CompZ age={age}/>
     </>
}

export default CompV;