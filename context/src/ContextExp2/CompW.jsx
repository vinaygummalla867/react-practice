import React from 'react';
import CompX from './CompX';
import CompY from './CompY';


const CompW = ({age})=>{
    return <>
        <CompX age = {age} />
        <CompY />
    </>
    
}

export default CompW;