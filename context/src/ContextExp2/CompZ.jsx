import React, { useContext } from "react";
import { appContext } from "./CompU";

const CompZ = ({age})=>{
   let contextvalue = useContext(appContext);
    return <>
        <h3>CompZ with Props data...age:{age}</h3>
        <h3>CompZ with Context data...age:{contextvalue}</h3>
          
    </>
};

export default CompZ;