import React, { useContext } from 'react';
import { appContext } from './CompU';


const CompY = ()=>{
    let contextvalue = useContext(appContext);
    return  <h3>{`CompY with context data, Age:${contextvalue}`}</h3>
       
}




export default CompY;