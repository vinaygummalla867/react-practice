const PromiseContainer = (x)=>{
    const a = new Promise((resolve,reject)=>{
        setTimeout(()=>{
            console.log("SetTimeout implementation");
            if(x%2 === 0){
                resolve("Even number so resolved");
            }
            reject("Please Send Even Number")
            
        },1000);
    });
    console.log('I am a statement executed before returning a promise');
    return a;
}
var x = PromiseContainer(2);
var y = PromiseContainer(3);
x.then((data)=>{console.log('After resolving the promise',data)}).catch((err)=>{console.log("error")});
y.then((data)=>{console.log('After resolving the promise',data)}).catch((err)=>{console.log("error")});

// new Promise((resolve,reject)=>{});