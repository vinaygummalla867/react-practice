import React, { useContext } from 'react';
import { myContext } from './CompA';


const CompD = ({data})=>{
    const val = useContext(myContext);
	return  <h3>{`I am Component D and got data through context:${val}%`}</h3>
          
};

export default CompD;