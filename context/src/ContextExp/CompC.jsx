import React, {useContext} from 'react';
import {myContext, secondContext} from './CompA';
import CompD from './CompD';

const CompC = ({data})=>{
    const x = useContext(myContext);
    const y = useContext(secondContext);

    return (<>
               <h3>{`I am Component C and I got data from A through context:${x}-${y}`}</h3>
                <h3>{`I am ComponentC and I got data from A through Prop Drilling:${data}%`}</h3>
                <CompD/>
  
    </>
    )
}

export default CompC;