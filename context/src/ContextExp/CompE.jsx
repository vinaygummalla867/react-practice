import React, { useContext } from 'react';
import { appContext } from '../ContextExp2/CompU';
const CompE = ()=>{
    let data = useContext(appContext);
    return  <h3>This is CompE with Context data...age:{data}</h3>

        

}

export default CompE;