import React, {useContext} from 'react';
import CompC from './CompC';
import { secondContext } from './CompA';



const CompB = ({data})=>{
	const z = useContext(secondContext);
	return  <>
			<h3>I am the data from Context A:{z}</h3>
			<CompC data={data}/></>
};

export default CompB;