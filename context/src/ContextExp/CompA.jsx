import React from 'react';
import CompB from './CompB';


export const myContext = React.createContext();
export const secondContext = React.createContext();

const CompA = ()=>{
	return <myContext.Provider value={40} > <secondContext.Provider value={50}> <CompB data={70}/> </secondContext.Provider>	 </myContext.Provider>
}
export default CompA;


// the 'value' attribute is fixed for passing data through context and it only takes 1 value, 
//but the value can also be array, object 