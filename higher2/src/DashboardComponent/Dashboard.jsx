import HigherOrderComponent from "../HigherOrderComponent/HigherComponent"
import React from 'react';

class Dashboard extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
    const products = this.props.products;
    return products.map((item,index)=>{
        return <p key={index}>{item.brand} costs {item.price} and is found in the {item.type} section of the store</p>
    });}
};


export default HigherOrderComponent(Dashboard);