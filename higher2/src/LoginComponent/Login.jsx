import React from 'react';

export default class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    /*submitForm = (e)=>{
        e.preventDefault();
        this.props.submitForm(this.state.username);
    }*/

    inputChange = (xyz,input) =>{
        this.setState({[input]:xyz.target.value});
    }

    render(){
      /*  return <div>
            <form onSubmit={(e)=>this.submitForm(e)}>
                <p>Enter Username</p>
                <input type='text' value={this.state.username} onChange={(e)=>{this.setState({username:e.target.value})}}/>
                <p>Enter Password</p>
                <input type='password' value={this.state.password} onChange={(e)=>{this.setState({password:e.target.value})}}/>
                <br/><input type='submit' value='submit'/>
            </form>
        </div>*/
        return <div>
        <form onSubmit={(e)=>{this.props.submitForm(this.state.username,e)}}>
            <p>Enter Username</p>
            <input type='text' value={this.state.username} onChange={(e)=>this.inputChange(e,'username')}/>
            <p>Enter Password</p>
            <input type='password' value={this.state.password} onChange={(e)=>this.inputChange(e,'password')}/>
            <br/><input type='submit' value='submit'/>
        </form>
    </div>
    }
}