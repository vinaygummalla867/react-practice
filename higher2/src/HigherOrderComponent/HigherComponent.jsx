import React from 'react';
import Login from '../LoginComponent/Login';
const HigherOrderComponent = (PassedComponent)=>{

    class Hoc extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                username:''
            };
        }
        submitForm = (username,e)=>{
            //e.preventDefault();
            this.setState({username});
            //console.log('Hoc',this.state.username);
            localStorage.setItem('username',username);
        }
        render(){
            const username = localStorage.getItem('username');
            return username ? <>
            <p>Welcome {username}</p>
            <PassedComponent {...this.props}/>
            <button onClick={()=>{localStorage.setItem('username','');this.setState({username:''})}}>Logout</button>
            </> : <Login submitForm={this.submitForm}/>
        }
    }
    return Hoc
};

export default HigherOrderComponent;