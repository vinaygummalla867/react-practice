import React from 'react';

const HigherOrderDisplayComponent = (WrapperComponent)=>{ 
    class Hoc extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                abc:"surya"
            };

        }
        render(){
            console.log('Hoc components',this.props);
            return<div style={{border:"1px solid"}}>
                <WrapperComponent {...this.props} abc={this.state.abc}/>
            </div>

            
        }
    }
    return Hoc
};
export default HigherOrderDisplayComponent;