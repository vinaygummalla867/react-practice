import React from 'react';
import DisplayList from '../displaylist/displaylist';
import HigherOrderDisplayComponent from '../HigherOrderDisplay/HigherOrderDisplayComp';
 class ListItems extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            products:[{brand: 'Adidas',price: 2300,type: 'Footwear',quantity: 0},
                      {brand: 'Nike',price: 3500,type: 'Footwear',quantity: 0},
                      {brand: 'UCB',price: 3000,type: 'Clothing',quantity: 0},
                      {brand: 'Safari',price: 1000000,type: 'SUV',quantity: 0},
                      {brand: 'Harrier',price: 1400000,type: 'SUV',quantity: 0},
                      {brand: 'Pepsi',price: 50,type: 'Beverage',quantity: 0},
                      {brand: 'ThumsUp',price: 55,type: 'Beverage',quantity: 0}
                    
                    ]
        }
    }

    editList = (index)=>{
        this.setState((previousState)=>{
            const prevproducts = [...previousState.products];
            prevproducts[index].price = prevproducts[index].price + 300;
            return {products:prevproducts}
        });
    }
    removeItems = (index)=>{
        this.setState((previousState)=>{
            const prevproducts = [...previousState.products];
            prevproducts.splice(index,1);
            return {products:prevproducts}
        });
    }
    addItem = ()=>{
        this.setState((previousState)=>{
            const prevproducts = [...previousState.products];
            prevproducts.push({brand: 'Fanta',price: 75,type: 'Beverage',quantity: 0});
            return {products:prevproducts}
        });
    }
    increaseQuantity = (index)=>{
        this.setState((previousState)=>{
            const prevproducts = [...previousState.products];
            prevproducts[index].quantity +=1;
            return {products:prevproducts}
        });
    }
    decreaseQuantity = (index)=>{
        this.setState((previousState)=>{
            const prevproducts =[...previousState.products];
           // prevproducts[index].quantity = prevproducts[index].quantity === 0 ? 0 : prevproducts[index].quantity-1;
           prevproducts[index].quantity = prevproducts[index].quantity === 0 ? 0: prevproducts[index].quantity-1 ;
            return {products:prevproducts}
        });
    }
    render(){
        const products = [...this.state.products];

        /* const props = [...this.state.products,t] */
        return <><button onClick={this.addItem}>{`Add Items ${this.props.abc}`}</button>
        <DisplayList // Hoc containing Display list Component
                products={products} 
                editList={this.editList} 
                removeItems={this.removeItems}
                addItem ={this.addItem}
                increaseQuantity={this.increaseQuantity}
                decreaseQuantity={this.decreaseQuantity}
              /></>
    }
}
export default HigherOrderDisplayComponent(ListItems);