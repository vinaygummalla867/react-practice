import React from 'react';
import HigherOrderDisplayComponent from '../HigherOrderDisplay/HigherOrderDisplayComp';

class DisplayList extends React.Component{
    constructor(props){
        super(props);

    }
    render(){
        const btnStyle = {margin:'5px'}; 
        return <>
        {this.props.abc}
        <div style={{margin:'auto',border:'1px solid'}}> {this.props.products.map((item,index)=>{
            return <p style={{color:'blue',margin:'10px',backgroundColor:'yellow'}}>You are going to buy <span style={{color:'red'}}>{item.brand} </span>{item.type} for {item.price} and you bought {item.quantity}
             <button style={btnStyle} onClick={()=>{this.props.editList(index)}}>Edit</button>
             <button style={btnStyle} onClick={()=>{this.props.removeItems(index)}}>Remove</button>
             <button style={btnStyle} onClick={()=>{this.props.increaseQuantity(index)}}>+</button>
             <button style={btnStyle} onClick={()=>{this.props.decreaseQuantity(index)}}>-</button>
             </p>
        })
     } </div> </>
    
    
    }
}

export default HigherOrderDisplayComponent(DisplayList); // returns Hoc(defined inside) and that will be exported