import React from 'react';

const HigherOrderComponent = (ParameterComponent)=>{
    class Hrc extends React.Component{
        constructor(props){
            super(props);
            this.state = {id:''};

        }
        
        render(){
            return <><ParameterComponent {...this.props} id={this.state.id}/>
           </>
        }
    }
    return Hrc
}

export default HigherOrderComponent;