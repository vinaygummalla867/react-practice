import React from 'react';

export default class Display extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    render(){
         /* return <p>{this.props.name} is {this.props.age} years old</p> */
        let items = this.props.items;
        return items.map((item,index)=>{
            return <p>{item.name} is {item.age} years old and works at {item.job}</p>
        })

    }
}