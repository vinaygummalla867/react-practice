import React from 'react';
import HigherOrderComponent from '../HigherOrderComponent/HigherOrderComponent';

 class ShowStudents extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            editMode:false,
            id:''
        };

    }
    saveId = ()=>{
        
    }
    render(){
        const {ide, editMode} = this.state;
        const {students, add, remove , editFee, incAge, decAge, id} = this.props;
        return  <><button onClick={add}>Add Student</button>
        
        { students.map((item,index)=>{
            return<p>{item.name} is learning {item.subject} and has Id {item.empId} and pays {item.fee} and is {item.age} years old
            <button onClick={()=>{remove(index)}}>Remove</button><button onClick={()=>{editFee(index)}}>Edit</button>
            <button onClick={()=>{incAge(index)}}>Age + </button><button onClick={()=>{decAge(index)}}>Age - </button>
            <button onClick={()=>{this.setState({editMode:!this.state.editMode},()=>{console.log(editMode)})}}>EditId</button>
            {editMode && <input type="text" value={ide} onChange={e=>this.setState({id:e.target.value})} /> &&
            <button>SaveId</button>
            }

            </p> 
        })}
         
         </>
    }
}
export default HigherOrderComponent(ShowStudents);