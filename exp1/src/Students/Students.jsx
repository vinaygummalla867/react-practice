import React from 'react';
import ShowStudents from '../ShowStudents/ShowStudents';

export default class Students extends React.Component{
    constructor(props){
        super(props);
        this.state = {students:[{name:"Surya", subject:'Js', empId:'Tc56',fee:1000, age:34}, 
        {name:'Divya', subject:'Java', empId:'Ab57',fee:1500 , age:23}, 
        {name:'Sekhar', subject:'Python', empId:'Zx22',fee:1200 , age:24}, 
        {name:'Vijay', subject:'Android' , empId:'Cy45',fee:2000 , age:29}, 
        {name:'Aman', subject:'C', empId:'Pu67',fee:2400 , age:31 } ]};
    }
    addStudent = ()=>{
       /* let studentsList = [...this.state.students];
       // console.log("oldlist",studentsList);
        studentsList.push({name:'Ajay', subject:'jsx', empId:'Pu95', fee:1600 });
       // console.log("updatedlist",studentsList);
        this.setState({students:studentsList});*/
        this.setState((previousState)=>{
            let students = [...previousState.students];
            students.push({name:'Ajay', subject:'jsx', empId:'Pu95', fee:1600, age:26 });
            return {students}
        });
    }
    incAge = (index)=>{
        this.setState((previousState)=>{
            let students = [...previousState.students];
            students[index].age +=1;
            return {students}
        });
    }
    decAge = (index)=>{
        this.setState((previousState)=>{
            const students = [...previousState.students];
            students[index].age -=1;
            return {students}
        });
    }
    removeStudent = (index)=>{
       this.setState((previousState)=>{
           const students = [...previousState.students];
           students.splice(index,1);
           return {students}
       });
    }
    editFee = (index)=>{
        this.setState((previousState)=>{
            const students = [...previousState.students];
            console.log("oldlist",students);
            students[index].fee += 200;
            return {students:students}
        })
    }
    
    render(){
        const students = [...this.state.students];
        return <>
        <ShowStudents students = {students} remove ={this.removeStudent} add = {this.addStudent} editFee = {this.editFee} incAge = {this.incAge}
        decAge = {this.decAge} 
        
        />
            
        </>
    }
}