import React from 'react';
import Input from '../InputComponent/Input';
import './SignUp.css';

export default class SignUp extends React.Component{
    constructor(props){
        super(props);
       
    }


    render(){

        return <><div className='form-container'>
                <h2>Sign Up Form</h2>
                <form className='form-element' onSubmit={this.props.submit}>
                    <label htmlFor='fname'>First Name:</label>
                    <Input type='text' value={this.props.fName} className='form' id='fname' change={this.props.fnChange}/><br/>
                    <span>{this.props.fnError === true ? 'Your name can only contain alphabets':''}</span><br/>
                    <label htmlFor='lname'>Last Name:</label>
                    <Input type='text' value={this.props.lName} className='form' id='lname' change={this.props.lnChange}/><br/>
                    <span>{this.props.lnError === true ? 'Your name can only contain alphabets':''}</span><br/>
                    <label htmlFor='email'>Enter Email:</label>
                    <Input type='email' value={this.props.email} className='form' id='email' change={this.props.emailChange}/><br/>
                    <span>{this.props.emailError === true ? 'Enter a valid Email Id':''}</span><br/>
                    <label htmlFor='pwd'>Choose a Password: </label>
                    <Input type='password' value={this.props.password} className='form' id='pwd' change={this.props.pwdChange}/><br/>
                    <span>{this.props.pwdError === true ? 'Your password should be of at least 8 characters in length and must contain at least one number, one uppercase,one lowercase and one special character':''}</span><br/>
                    <input type='submit' value='Submit'/>{`   `}
                    <input type='reset'/>
                </form>
            </div></>
    }
}