import React from 'react';

export default class Input extends React.Component{
    constructor(props){
        super(props);
        
    }


    render(){

        return <input type={this.props.type} id={this.props.id} className={this.props.className} value={this.props.value} onChange={this.props.change}/>
    }
}