import React from 'react';
import SignUp from '../SignUp/SignUp';
import Dashboard from '../Dashboard/dashboard';
import {emailPattern,pwdPattern,fnPattern,lnPattern} from '../Utilities/Utils';
export default class AppComponent extends React.Component{
    constructor(){
        super();
        this.state={
            currentPage:'signup',
            firstName:'',
            lastName:'',
            email:'',
            password:'',
            fnError:false,
            lnError:false,
            emailError:false,
            pwdError:false


        };
    }
    
    fnChange=(event)=>{
        this.setState({firstName:event.target.value});

    }
    lnChange=(event)=>{

        this.setState({lastName:event.target.value});
    }
    emailChange=(event)=>{
        this.setState({email:event.target.value});
    }
    pwdChange=(event)=>{
        this.setState({password:event.target.value});
    }
    navigatePage =()=>{
        this.setState({currentPage:this.state.currentPage === 'signup' ? 'dashboard':'signup'});
    }
    formSubmit=(event)=>{
        event.preventDefault();
        
        this.setState({fnError:!fnPattern.test(this.state.firstName)},()=>console.log('fn',fnPattern.test(this.state.firstName)));
        this.setState({lnError:!lnPattern.test(this.state.lastName)},()=>console.log('ln',lnPattern.test(this.state.lastName)));
        this.setState({emailError:!emailPattern.test(this.state.email)},()=>console.log('email',emailPattern.test(this.state.email)));
        this.setState({pwdError:!pwdPattern.test(this.state.password)},()=>console.log('pwd',pwdPattern.test(this.state.password)));
       if(fnPattern.test(this.state.firstName) === true && lnPattern.test(this.state.lastName) === true && emailPattern.test(this.state.email) === true  && pwdPattern.test(this.state.password) ===true)
        {this.setState({currentPage:'dashboard'});}
    }

    render(){

        return <>
        {this.state.currentPage === 'signup' ? <SignUp
            currentPage={this.state.currentPage}
            fName={this.state.firstName}
            lName={this.state.lastName}
            email={this.state.email}
            password={this.state.password}
            fnError={this.state.fnError}
            lnError={this.state.lnError}
            emailError={this.state.emailError}
            pwdError={this.state.pwdError}
            fnChange={this.fnChange}
            lnChange={this.lnChange}
            emailChange={this.emailChange}
            pwdChange={this.pwdChange}
            submit={this.formSubmit}
            
        />: <Dashboard fname={this.state.firstName} lname={this.state.lastName} mail={this.state.email} password={this.state.password}/> }
       </>
    }
}