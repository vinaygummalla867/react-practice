
import React from 'react';
import DisplayUser from '../DisplayUser/DisplayUser';
export default class Main extends React.Component{
    constructor(props){
        super(props);
        this.state = {toggle:false};
    }
    toggleChange = ()=>{
        this.setState({toggle:!this.state.toggle});
    }
    render(){
        return <><input type="checkbox" onChange={this.toggleChange} />
       <DisplayUser   toggle={this.state.toggle} />
        </>
        
    }
}