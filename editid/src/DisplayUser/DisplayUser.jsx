import React from 'react';
import HigherComponent from '../HigherComponent/HigherComponent';

 class DisplayUser extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            username:''
        };

    }
    render(){
        const {toggle, editMode, editModeChange} = this.props;
        return <><p>Username {toggle && !editMode && <button onClick={editModeChange}>Edit</button>}</p>
           <p>  {toggle && editMode ?<> <input type="text" value = {this.state.username} 
           onChange ={(e)=>{this.setState({username:e.target.value})}} />
           <button onClick={editModeChange}>Save</button>
            </>  : this.state.username} 
                           </p>
</>
    }
}

export default HigherComponent(DisplayUser);