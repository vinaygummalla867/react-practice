import React from 'react';
import HigherComponent from '../HigherComponent/HigherComponent';

class DisplayData extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            id:''
        }
    }
    render(){
        const students = [...this.props.students];
        return students.map((item,index)=>{
           return <p>{item.name} has id of {item.empId}
           <input type="text" value={this.state.id} onChange={(e)=>{this.setState({id:e.target.value})}}/>
           {this.state.id}
           </p>
        })
    }
}

//export default HigherComponent(DisplayData);