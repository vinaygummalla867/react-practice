import React from 'react';

const HigherComponent = (ParameterComponent)=>{
    class Hoc extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                editMode:false
            };
    
        }
        editModeChange = ()=>{
            this.setState({editMode:!this.state.editMode});
        }
        render(){
            return <ParameterComponent {...this.props} editModeChange = {this.editModeChange}  editMode = {this.state.editMode}  />
        }
    }
    return Hoc
};

export default HigherComponent;

