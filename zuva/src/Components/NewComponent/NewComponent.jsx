import React from "react";

function IncrementedValue(props){
    return <div>The multiplied value is {props.incrementedValue * props.yValue}</div>
}


export default IncrementedValue;