import React from  'react';
import ClassComp from "./classwrap";
import XY from"./NewComponent/NewComponent";
function FunComponent(){
    return <div class="container" style={{backgroundColor:"red",width:'300px',border:'1px solid'}}>This is the functional component element
    <ClassComp/>
    <XY incrementedValue={20} yValue={60} zValue={32}/>
    </div>
}
export default FunComponent;