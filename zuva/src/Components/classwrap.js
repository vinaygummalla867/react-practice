import React, { useEffect, useState } from 'react';
import { render } from 'react-dom';
//import FunComponent from "./";
import XY from "./NewComponent/NewComponent";
/*export class ClassComp extends React.Component{
    constructor(props){
        super(props);
        this.state = {x:10,y:20,z:40};
        
        this. multiplyWithSetState = this. multiplyWithSetState.bind(this);
    }
   /* increment(){
       // this.setState({x:this.state.x + 10});  //method from React.Component
       this.state.x = this.state.x + 10;
       console.log("In increment method",this.state.x);
    }

    multiplyWithSetState(){
        this.setState({x: this.state.x + 10}); //setState is method from React component 
        
    }
    render(){
        console.log("In render method",this.state.x);
        return <>
         <div class="class-container" style={{backgroundColor:'blue',margin:'5px',width:'300px',minHeight:'80px',border:'2px solid blue'}}>
         This is the class component element </div>
         {/* <button onClick={this.increment}>Increment</button>}
          <button onClick={this.multiplyWithSetState}>multiplyWithSetState</button>
          <XY incrementedValue={this.state.x} yValue={this.state.y}/> {/*HTML attributes }
         </>

   
        
    }
    
} */



const ClassComponent = ()=>{
    //let x,y;
    const [x,setX] = useState(0); // [statevalue,UpdaterFunction]
    const [y,setY] = useState(2);

    const multiplyWithUseStateX = ()=>{
        setX(x+10);
       // setY(y+2);
    }
    const multiplyWithUseStateY = ()=>{
        //setX(x+10);
        setY(y+2);
    }

    useEffect(()=>{
        console.log("I am executing due to useEffect Hook always running after render or rerender");
        return ()=>{console.log('Component will unmount');}
});
    useEffect(()=>{console.log("I am executing due to  useEffect Hook (mount effect)")},[]);
    useEffect(()=>{console.log("I am executing due to useEffect Hook(mount and update effect)")},[x]);

    return <> <div class="class-container" style={{backgroundColor:'blue',margin:'5px',width:'300px',minHeight:'80px',border:'2px solid blue'}}>
    This is the class component element </div>
    <button onClick={multiplyWithUseStateX}>multiplyWithSetStateX</button>
    <button onClick={multiplyWithUseStateY}>multiplyWithSetStateY</button>
          <XY incrementedValue={x} yValue={y}/></>
} 
export default ClassComponent;