import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import reportWebVitals from './reportWebVitals';
import FunComponent from './Components';
//import {ClassComp} from './Components/classwrap';

/*const child = React.createElement("div",{class:"child"}, "This is the child div element");
const child2 = React.createElement("p",{class:"child", style:{backgroundColor:"red"}}, "This is the child P element");
const parent = React.createElement("div",{class:"parent", style:{backgroundColor:"yellow", fontSize:"80px"}}, "This is the parent in the container", child, child2);
const container = React.createElement("div", {class:"container", style:{fontSize:"100px", backgroundColor:"blue"}},"This is the container element",parent);
*/
ReactDOM.render(
 /*<React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')//
   container, 
   document.getElementById('root')*/
  <FunComponent/>,
   document.getElementById('root')

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
