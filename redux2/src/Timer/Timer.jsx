import React from "react";
import { connect } from "react-redux";
import { incrementAction, decrementAction } from "../Redux/Actions";
const Timer = (props)=>{
    const {timer, increment, decrement} = props;
  
    return <>
        <p>The timer value:{timer}</p>
        <button onClick = {()=>{increment()}} >+</button>
        <button onClick={()=>{decrement()}} >-</button>
    </>
}

const mapStateToProps = (state)=>({
    timer:state.timer.timer
});

const mapDispatchToProps = (dispatch)=>({
    increment:()=>{dispatch(incrementAction())},
    decrement:()=>{dispatch(decrementAction())}
})

const Hoc = connect(mapStateToProps, mapDispatchToProps);

export default Hoc(Timer);