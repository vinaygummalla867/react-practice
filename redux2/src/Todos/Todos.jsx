import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchTodos } from "../Redux/Actions";

const Todos = (props)=>{
    const {todos, fetchTodos} = props;
    useEffect(()=>{
        fetchTodos();
    }, [] );

    return <>
    <ul>{
        todos.map((todoItem, index)=><li key ={index} > {todoItem.title} </li>)
        }</ul>
    </>

}

const mapStateToProps = (state)=>({
    todos:state.todos
});

const mapDispatchToProps = (dispatch)=>({
    fetchTodos:()=>{dispatch(fetchTodos())}
});

const Hoc = connect(mapStateToProps, mapDispatchToProps);

export default Hoc(Todos);