import { createStore } from "redux";
import rootReducer from "./Reducer";
import ThunkMiddleware  from "redux-thunk";
import { applyMiddleware } from "redux";

const store = createStore(rootReducer, applyMiddleware(ThunkMiddleware));

export default store;