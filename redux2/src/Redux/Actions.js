export const incrementAction = ()=>({type:"Increment"});
export const decrementAction = ()=>({type:"Decrement"});


export const fetchTodos = ()=>{

    return (dispatch)=>{
        try {
            fetch("https://jsonplaceholder.typicode.com/todos").then(res=>res.json()).then(data=>dispatch({
                type:"Add-Todos", payload:data
            }))
        }
        catch (e){console.log(e)}
    }

}