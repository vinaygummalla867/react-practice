import { combineReducers } from "redux";


const timerReducer = (state={timer:0}, action)=>{
    switch(action.type){
        case "Increment": return {...state, timer:state.timer + 1};
        case "Decrement": return {...state, timer:(state.timer === 0 ? 0: state.timer - 1)  };
        default: return state
    }
}


const todosReducer = (state=[], action)=>{
    switch(action.type){
        case "Add-Todos": return [...state, ...action.payload ]
        default: return state
    }
}

const rootReducer = combineReducers({ timer:timerReducer, todos:todosReducer });

export default rootReducer;