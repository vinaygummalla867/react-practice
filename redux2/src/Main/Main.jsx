import React from "react";
import Timer from "../Timer/Timer";
import Todos from "../Todos/Todos";

const Main = ()=>{
    return <><Timer/>
    <Todos/>
    </>
}

export default Main;