import React, { useEffect, useState, useRef, createRef } from "react";
import { connect } from "react-redux";
import  fetchTodos from "../Actions/actions";

// Create a reference,
// pass that reference to actual dom element
// use that reference to do some operation
/*constructor(){
    this.divRef = createRef();
}*/

const Todos = (props)=>{
    const [x, setX] = useState();// doubt
  //  const {todos, fetchTodos} = props;
  //setX(props.todos);
    const divRef = useRef();
    console.log(x);
    useEffect(()=>{
        //console.log(props.todos);
        props.fetchTodos();// only runs once cos of 2nd parameter "[]"
        //setTodos(props.todos); //doubt, how to use setTodos
        console.log(divRef.current.innerText);
        
    }, []);// what is happening if I give props.todos as a 2nd parameter

    return <>
            <h2 ref={divRef}>Todos List</h2>
        <ul>{props.todos.map((item,index)=><li key = {index} >{item.title}</li>)}</ul>
    </>


}

const mapDispatchToProps = (dispatch)=>({           //doubt
    fetchTodos:()=>{dispatch(fetchTodos())}
});

const mapStateToProps = (state)=>({
    todos:state
});

const Hoc = connect(mapStateToProps, mapDispatchToProps);

export default Hoc(Todos);