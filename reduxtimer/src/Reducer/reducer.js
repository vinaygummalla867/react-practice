const todosReducer = (state=[], action)=>{
    switch(action.type){
        case "ADD-TODOS": return [...state, ...action.payload];
        default: return state
    }
    
}
 

export default todosReducer;