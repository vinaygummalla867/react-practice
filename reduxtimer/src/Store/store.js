import { createStore, applyMiddleware } from "redux";
import reduxReducer from "../Reducer/reducer";
import  ThunkMiddleware  from "redux-thunk";

const reduxStore = createStore(reduxReducer,applyMiddleware(ThunkMiddleware));

export default reduxStore;