import React from 'react';
import '../LoginPage/LoginPage.css';


export default class Teachers extends React.Component{
    constructor(props){
        super(props);
        this.state = {
                        userName:'',
                        password:'',
                        userNameError:false,
                        passwordError:false

        };
    }
    changeUserName =(event)=>{
            this.setState({userName:event.target.value},()=>{console.log(this.state.userName)});//{userName:'V'..'Vi'..'Vin'..'Vina'..'Vinay'}
           // console.log(this.state.userName);//V..Vi..Vin..Vina...Vinay
    }
    changePassword=(event)=>{
        this.setState({password:event.target.value},()=>{console.log(this.state.password)});
    }
    submitForm=(event)=>{
        event.preventDefault();
        if(this.state.userName.length<6){
            this.setState({userNameError:true});
        }
        if(this.state.password.length<6){
            this.setState({passwordError:true});
        }
    }
    render(){
       // const stylee = {width:'40%',height:'40%',margin:'auto',marginTop:'5%',border:'1px soild',borderRadius:'4%', backgroundColor:'yellow'};
        //const style ={padding:'8vw'};
        
        return <>
            <h1>Login Page</h1>
            {/*<div style={stylee}>
            <form style={style} onSubmit={this.submitForm}>*/}
            <div className='form-container'>
                <form onSubmit={this.submitForm}>
                    <label>Enter Teacher ID:<input value={this.state.userName} type='text' id='TeachId' className='q' placeholder='ID' onChange={this.changeUserName} required></input></label>
                    <br/><br/>
                    <span>{this.state.userNameError ? 'Username must contain at least 6 characters':''}</span>{/*Conditional Rendering */}
                    <label>Enter Password: <input value={this.state.password} type='password' id='Teachpwd'  className='q' placeholder='Password' onChange={this.changePassword} required></input></label>
                    <br/> <br/>
                    <span>{this.state.passwordError ? 'Password must contain at least 8 characters':''}</span><br/>
                    <input type='submit' value='Submit' ></input></form>
            </div></>
    }
}