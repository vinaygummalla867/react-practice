import React from 'react';

import AB from '../TeacherFunction/TeacherFunction';


export default class TeacherClass extends React.Component{
    constructor(props){
        super(props);
       // this.updateTeachers=this.updateTeachers.bind(this);
        this.state = {
            teachers:[{name:"Surya",subject:'Js',empId:'Tc56'},
            {name:'Divya',subject:'Java',empId:'Ab57'},
            {name:'Sekhar',subject:'Python',empId:'Zx22'},
            {name:'Vijay',subject:'Android',empId:'Cy45'},
            {name:'Aman',subject:'C',empId:'Pu67' }
        ]
        };

    }

    updateTeachers =()=>{
        let teachersList = this.state.teachers;
        teachersList.push({name:'Dharam', subject:'DAA', empId:'GGu30'});
        this.setState({teachers:teachersList});
    }


    render(){
        const style={backgroundColor:'green', fontSize:'1.5rem', border:'1.5px solid black',width:'80vw', marginTop:'2vh'};
        const list =this.state.teachers.map((item)=><AB name={item.name} subject={item.subject} empId={item.empId} />)
        return <>
        
        <div style={style}>This is the teachers class component</div>
       
        <h1>This is Teachers function</h1>
        {list}

        {`Abcd ${style.backgroundColor}`}
        

        <button onClick={this.updateTeachers}>Add Teachers</button>
        </>
    }
}