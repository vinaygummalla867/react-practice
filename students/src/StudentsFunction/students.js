import React from 'react';

function Students(props){
    const style ={backgroundColor:"blue", marginTop:'2vh',width:'80vw', border:"1px solid yellow", fontSize:"1.2rem"};
    return <div style ={style}>{props.name} has a roll number of {props.rollno} and belongs to section {props.section}</div>
};



export default Students;


