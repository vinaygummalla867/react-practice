import React from 'react';
import Students from "../StudentsFunction/students";

export default class StudentsClass extends React.Component{
    constructor(props){
        super(props);
        this.addStudent=this.addStudent.bind(this);
        this.state ={
            students:[{name:"vinay",rollno:17, section:"A"},{name:"ajay", rollno:1 ,section:"B"},{name:"Akshay" ,rollno:7, section:"A"},{name:"jay", rollno:9 ,section:"A"}]
    };
    };
    addStudent =()=>{
        let studentsList = [...this.state.students];
        studentsList.push({name:'Akhil',rollno:32,section:'C'});
        this.setState({students:studentsList});
        console.log(this.state.students);
    }
    render(){

        const style = {backgroundColor:"red", marginTop:'2vh', width:'100vw', border:"1px solid green", fontSize:"1.4rem"};
        return <><div style={style}>This info is for class {this.props.class} students studying in {this.props.school} in the state of {this.props.state}</div>
        {this.state.students.map((item)=><Students name={item.name} rollno={item.rollno} section={item.section}/>)}

        <button onClick={this.addStudent}>AddStudent</button> {/*onclick method shouldn't be called
        but just defined since we are passing dynamically, calling will execute the method in the dynamic process
         and the resulting return value only will remain as onclick=value
        */}
        </>
    }
}