import React from 'react';


function TeacherFunc(props){
    const style={backgroundColor:'Yellow', fontSize:'1.5rem', border:'1px solid black', marginTop:'2vh',textAlign:'center',width:'80vw'};
    return <div style={style}> 
        {props.name} teaches {props.subject} and has an Id of {props.empId}</div>
    
}

export default TeacherFunc;