import React from 'react';


export default class DisplayLists extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    render(){
        const {name, subject} = this.props;
        
        return  <p>{name} teaches {subject}</p>
     
    }
}