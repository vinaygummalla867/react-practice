import React from 'react';

let colors = ['red','yellow','green','blue','orange','purple','violet'];
export default class ChangeColor extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            color:''
        };
    }
    randomNumber = (min, max)=>{
        return Math.floor(Math.random() * (max - min) + min);
    }
    componentDidMount(){
      this.intervalId = setInterval(()=>{
            let index = this.randomNumber(0,6);
            this.setState(()=>{
                return {color:colors[index]}
            });
       },500);
    }
    stopChange = ()=>{
        clearInterval(this.intervalId);
        this.setState(()=>{
            return {color:''}
        });
    }
    render(){
        
        return <button onClick={this.stopChange} style={{backgroundColor:this.state.color}}>Change Background</button>
    }
}