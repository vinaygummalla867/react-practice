import React from 'react';


let colors = ['red','yellow','green','blue','orange','purple','violet'];
export default class ChangeBg extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                color:'',
                change:true
            };
        }
        randomNumber = (min, max)=>{
            return Math.floor(Math.random() * (max - min) + min);
        }
        start = ()=>{
          this.intervalId =  setInterval(()=>{
                let index = this.randomNumber(0,6);
            this.setState(()=>{
                return {color:colors[index]}
            });
            },500);
            
            this.setState({change:!this.state.change},()=>{console.log('startend',this.state.change);});
            
        }
        stop = ()=>{
            clearInterval(this.intervalId);
            this.setState({color:'',change:!this.state.change},()=>{console.log('stopend',this.state.change);});
            
        }
        render(){
            return this.state.change ? <button onClick={this.start} style={{backgroundColor:this.state.color}}>ChangeBg</button> :
            <button onClick={this.stop} style={{backgroundColor:this.state.color}}>ChangeBg</button>
        }

}