import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import Vinay from './App';
import reportWebVitals from './reportWebVitals';
//import AB from "./FunComponent";
import {ClassCompo} from './ClassComponent/classcomp'; 
import ChangeColor from './ChangeColor/ChangeColor';
import ChangeBg from './ChangeBg/ChangeBg';
import List from './DisplayList/DisplayList';


ReactDOM.render(
  <React.StrictMode>
    {/*<Vinay />*/}
   { /*<AB/>
   <ClassCompo yum="food" drink = "water" valuu={30}/>
   <ChangeColor/>
   */}
    <ChangeBg/>
    <List/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
