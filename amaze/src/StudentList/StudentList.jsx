import React from 'react';
import DisplayLists from '../DisplayList/DisplayList';

export default class List extends React.Component{
    constructor(props){
        super(props);
        this.state = {students:
            [{name:"Surya", subject:'Js', empId:'Tc56'}, 
             {name:'Divya', subject:'Java', empId:'Ab57'}, 
             {name:'Sekhar', subject:'Python', empId:'Zx22'}, 
             {name:'Vijay', subject:'Android' , empId:'Cy45'}, 
             {name:'Aman', subject:'C', empId:'Pu67' } ]
    }
    }
    
    render(){
       const students = [...this.state.students];

        return <>{students.map((item,index)=>{<DisplayLists name={item.name} subject={item.subject}/>})}</>
    }
};