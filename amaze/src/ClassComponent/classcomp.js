import React from 'react';
import YZ from '../FunComponent';


export class ClassCompo extends React.Component{
    constructor(props){
        super(props);

    };
    render(){
        return <div class="classcompo" style={{width:'70%', height:'30vh', backgroundColor:'blue'}}> 
        This is a class component element and we have {this.props.yum} and {this.props.drink} at only the price of {this.props.valuu};
        <YZ get={10} take="victor"/>
        </div>
    };
};