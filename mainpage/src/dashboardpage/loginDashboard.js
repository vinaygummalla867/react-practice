import React from 'react';


export default class LoginDashboard extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
       return <>
        <button onClick={this.props.toLogin} >Logout</button>
       <h2>Login Dashboard</h2>
       <p>Welcome to the Login Dashboard page {this.props.fname} {this.props.lname}</p>
       <p>You've successfully logged in to your account with us using your email: {this.props.email}</p>
       </>
}

}