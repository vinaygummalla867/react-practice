import React from 'react';


export default class Dashboard extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
       return <>
        <button onClick={this.props.toSignup} onDoubleClick={this.props.toLogin}>Signout/Login</button>
       <h2>Dashboard</h2>
       <p>Welcome to the Dashboard page {this.props.fname} {this.props.lname}</p>
       <p>You've successfully created an account with us using your email: {this.props.email}</p>
       </>
}

}