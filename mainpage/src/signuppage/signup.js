import React from 'react';
import Input from '../inputpage/input';
import './signup.css';


export default class Signup extends React.Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        console.log('Signuppage Component did mount properly'); 
    }
    componentDidUpdate(){
        console.log('Signuppage Component did update properly'); 
    }
    componentWillUnmount(){
        console.log('Signuppage Component will unmount'); 
    }

    render(){
       

        return <>
        <button onClick={this.props.toLogin}>Login</button>
        <div className='signup-container'>
            <h1>{this.props.page}</h1>
            <form className='form-element' onSubmit={this.props.signUpSubmit}>
                <label forName='fname'>Enter Your First Name:</label>
                <Input type='text' id='fname' name='q' value={this.props.fname} change={this.props.fnChange}/><br/>
                <span>{this.props.fnError === true ? 'Your name cannot contain numbers':''}</span><br/>
                <label forName='lname'>Enter Your Last Name:</label>
                <Input type='text' id='lname' name='q' value={this.props.lname} change={this.props.lnChange}/><br/>
                <span>{this.props.lnError === true ? 'Your name cannot contain numbers':''}</span><br/>
                <label forName='email'>Enter Your Email:</label>
                <Input type='email' id='email' name='q' value={this.props.email} change={this.props.emailChange}/><br/>
                <span>{this.props.emailError === true ? 'Your email format is incorrect':''}</span><br/>
                <label forName='pwd'>Choose a Password:</label>
                <Input type='password' id='pwd' name='q' value={this.props.password}  change={this.props.pwdChange}/><br/>
                <span>{this.props.pwdError === true ? 'Your password must be min characters long with at least one uppercase, lowercase, number, special characters':''}</span><br/>
                <Input type='submit' value='Sign Up'/>

                <input type='reset' />
            </form>
        </div>
        </>
    }

}







































