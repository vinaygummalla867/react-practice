import React from 'react';
import Input from '../inputpage/input';
import './login.css'


export default class Login extends React.Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        console.log('Loginpage Component did mount properly'); 
    }
    componentDidUpdate(){
        console.log('Loginpage Component did update properly'); 
    }
    componentWillUnmount(){
        console.log('Loginpage Component will unmount'); 
    }

    render(){
       return <>
       <button onClick={this.props.toSignup}>SignUp</button>
       <div className='login-container'>
       <h1>{this.props.page}</h1>
       <form className='form-element' onSubmit={this.props.loginSubmit}>
           
           <label forName='email'>Enter Your Email:</label>
           <Input type='email' id='email' name='q' value={this.props.email} change={this.props.emailChange}/><br/>
           <span>{this.props.emailError === true ? 'Your email format is incorrect':''}</span><br/>
           <label forName='pwd'>Enter Password:</label>
           <Input type='password' id='pwd' name='q' value={this.props.password}  change={this.props.pwdChange}/><br/>
           <span>{this.props.pwdError === true ? 'Your password is Incorrect':''}</span> <br/>
           
           <Input type='submit' value='Log In'/>

        
       </form>
   </div>
   </>
    }

}