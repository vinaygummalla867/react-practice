import React from 'react';
import Signup from '../signuppage/signup';
import Dashboard from '../dashboardpage/dashboard';
import {emailPattern,pwdPattern,fnPattern,lnPattern} from '../utils';
import Login from '../loginpage/login';
import LoginDashboard from '../dashboardpage/loginDashboard';


export default class Main extends React.Component{
    constructor(){
        super();
        this.state ={
            currentPage:'Signup',
            firstName:'',
            lastName:'',
            email:'',
            password:'',
            pwdError:false,
            emailError:false,
            fnError:false,
            lnError:false
        }
    }
    signUpSubmit = (event)=>{
        event.preventDefault();
        this.setState({fnError:!fnPattern.test(this.state.firstName)});
        this.setState({lnError:!lnPattern.test(this.state.lastName)});
        this.setState({emailError:!emailPattern.test(this.state.email)});
        this.setState({pwdError:!pwdPattern.test(this.state.password)});


       
       if(fnPattern.test(this.state.firstName) === true && lnPattern.test(this.state.lastName) === true  && emailPattern.test(this.state.email) === true && pwdPattern.test(this.state.password) === true){
        this.setState({currentPage:'Dashboard'});
    }
    
    }
    fnChange = (event)=>{
        this.setState({firstName:(event.target.value)});
    }
    lnChange = (event)=>{
        this.setState({lastName:(event.target.value)});
    }
    emailChange = (event)=>{
        this.setState({email:(event.target.value)});
    }
    pwdChange = (event)=>{
        this.setState({password:(event.target.value)});
    }
    currentPageChange=(page)=>{
        this.setState({currentPage:page});
    }
    componentDidMount(){
        console.log('Mainpage Component did mount properly'); 
    }
    componentDidUpdate(){
        console.log('Mainpage Component did update properly'); 
    }
    componentWillUnmount(){
        console.log('Mainpage Component will unmount'); 
    }
    navigateToSignUp =()=>{
        
        
        this.setState({password:'',firstName:'',lastName:'',email:''});
        
        this.currentPageChange('Signup');
    }
    navigateToLogin =()=>{
        
        this.setState({email:'',password:''});
        this.currentPageChange('Login');
    }

    loginSubmit =(event)=>{
        event.preventDefault();
        this.setState({emailError:!emailPattern.test(this.state.email),pwdError:!pwdPattern.test(this.state.password)});
       
        if(emailPattern.test(this.state.email) === true && pwdPattern.test(this.state.password) === true ){
            this.currentPageChange('LoginDashboard');
        }

    }
    render(){
        
         /* 
            return 
            this.state.currentPage === 'Signup' ? <Signup 
            page={this.state.currentPage} 
            fname={this.state.firstName}
            lname={this.state.lastName}
            email={this.state.email}
            password={this.state.password}
            pwdError={this.state.pwdError}
            emailError={this.state.emailError}
            fnError={this.state.fnError}
            lnError={this.state.lnError}
            fnChange={this.fnChange}
            lnChange={this.lnChange}
            emailChange={this.emailChange}
            pwdChange={this.pwdChange}
            formSubmit={this.formSubmit}
            navigateToSignUp={this.navigateToSignUp}

            />: <Dashboard fname={this.state.firstName} lname={this.state.lastName} email={this.state.email}/>
        */
              return <> {this.state.currentPage === 'Signup' && <Signup 
            page={this.state.currentPage} 
            fname={this.state.firstName}
            lname={this.state.lastName}
            email={this.state.email}
            password={this.state.password}
            pwdError={this.state.pwdError}
            emailError={this.state.emailError}
            fnError={this.state.fnError}
            lnError={this.state.lnError}
            fnChange={this.fnChange}
            lnChange={this.lnChange}
            emailChange={this.emailChange}
            pwdChange={this.pwdChange}
            signUpSubmit={this.signUpSubmit}
            toLogin={this.navigateToLogin}/>}
            
             {this.state.currentPage === 'Login' && <Login
            email={this.state.email}
            password={this.state.password}
            emailChange={this.emailChange}
            pwdChange={this.pwdChange}
            emailError={this.state.emailError}
            pwdError={this.state.pwdError}
            page={this.state.currentPage}
            loginSubmit={this.loginSubmit} 
            toSignup={this.navigateToSignUp}/>}

            {this.state.currentPage === 'LoginDashboard' &&
             <LoginDashboard fname={this.state.firstName} lname={this.state.lastName} email={this.state.email} toLogin={this.navigateToLogin}/>}
        
            {this.state.currentPage === 'Dashboard' && 
            <Dashboard fname={this.state.firstName} lname={this.state.lastName} email={this.state.email}
            toSignup={this.navigateToSignUp} toLogin={this.navigateToLogin}/>}</>
           
    }       
       /* {this.state.currentPage === 'Signup' ? <Signup/>:null}//using ternary operator
        {this.state.currentPage === 'Signup' && <Signup/>}//using AND 
        {this.state.currentPage === 'Login' && <Login/> }*/
}