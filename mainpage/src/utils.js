export const emailPattern = /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/;
export const pwdPattern = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
export const fnPattern = /^[a-zA-Z ]{2,30}$/;
export const lnPattern = /^[a-zA-Z ]{2,30}$/;