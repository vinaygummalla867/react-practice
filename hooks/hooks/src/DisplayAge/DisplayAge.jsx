import React from 'react';


export default function DisplayAge(props){
    const {age, xy, changeAge, changeClass} = props;
    return <><p>Ajay is {age} years old in {xy} class</p>
    <button onClick={changeAge} >ChangeAge</button><button onClick={changeClass}>ChangeClass</button>
    </>
}