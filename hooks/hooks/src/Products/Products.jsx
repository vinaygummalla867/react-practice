import React, { useEffect, useState } from 'react';
import DisplayPrice from '../DisplayPrice/DisplayPrice';

const Products = ()=>{
    let [shoePrice, setShoePrice] = useState(200);  
    let [glassPrice, setGlassPrice] = useState(160);

    useEffect(()=>{
        console.log('Executing on every render and re-render');
        return console.log('component will unmount1')
        
    });
    useEffect(()=>{
        console.log('Executing on the very first render');
        return console.log('component will unmount2')
    },[]);

    useEffect(()=>{
        console.log('Executes only when shoeprice changes');
        return console.log('component will unmount3')
    },[shoePrice]);
    useEffect(()=>{
        console.log('Executes only when glassprice changes');
        return console.log('component will unmount4')
    },[glassPrice]);

    const editShoePrice = ()=>{
        setShoePrice(shoePrice + 60);
    }
    const editGlassPrice = ()=>{
        setGlassPrice(glassPrice + 40);
    }

    return <><DisplayPrice shoePrice = {shoePrice} glassPrice = {glassPrice} />
             <button onClick = {editShoePrice}>ChangeShoe</button>
             <button onClick = {editGlassPrice}>ChangeGlass</button>
</>
};

export default Products;