import React, {useState, useEffect} from 'react';
import DisplayAge from '../DisplayAge/DisplayAge';
const FunctionComp = ()=>{
    const [age, setAge] = useState(12); 
    const [xy, changeXY] = useState(7);
const changeAge = ()=>{
    setAge(age+1);
  
}
useEffect(()=>{
    console.log("executing with setAge");
}, []);

useEffect(()=>{
    console.log("executing with changeXY");
}, []);

const changeClass = ()=>{
    changeXY(xy +2);
   
}
    return <DisplayAge age = {age} xy = {xy} changeAge ={changeAge} changeClass={changeClass} />
};

export default FunctionComp;