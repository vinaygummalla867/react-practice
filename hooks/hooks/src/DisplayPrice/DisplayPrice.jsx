import React from 'react';

export default function DisplayPrice(props){

    let {shoePrice, glassPrice} = props;
    return <><p>Shoe costs {shoePrice}</p>
            <p>Glasses cost {glassPrice}</p>
    </>
}