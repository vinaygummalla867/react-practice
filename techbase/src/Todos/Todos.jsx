import React from "react";
//import "./Todos.css" ;

class Todos extends React.Component{

    constructor(props){
        super(props);
        this.state = {click:false};
      //  this.liRef = React.createRef();
    }
    singleClick = ()=>{
        
        this.setState({click:!this.state.click});
      
        
    }

    componentDidMount(){
        //console.log("Mounted:", this.liRef.current.innerText);
    }
    componentDidUpdate(){
        //console.log("Updated:", this.liRef.current.innerText);
    }

    render(){
        const {title, id, userId, remove, index} = this.props;
        return <>
        {!this.state.click &&   <li  onClick = {()=>{this.singleClick()}}   >{title}</li>}
        
    {this.state.click && <>    <li  onDoubleClick= {()=>{remove(index);this.setState({click:!this.state.click});}} >{title}<sup>{id}{ userId}</sup></li></> }
        
        </>
    }

} 
    


export default Todos;