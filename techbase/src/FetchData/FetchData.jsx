import React from "react";
import Todos from "../Todos/Todos";

class TechBase extends React.Component{

    constructor(props){
        super(props);
        this.state = {todos:'', show:false, btn:true, click:false, todosTitle:'Fetch'};
        this.buttonRef = React.createRef();
       
    }

    fetchTodos = ()=>{
        fetch("https://jsonplaceholder.typicode.com/todos").then(res=>res.json()).then(res=>{this.setState({todos:res,todosTitle:this.state.todosTitle==='Fetch' ? 'ReFetch':'Fetch'}, console.log(this.state.todos))}).catch((err)=>{console.log("Error")})
        this.setState({show:!this.state.show});
    }
    componentDidUpdate(){
        console.log(this.buttonRef.current.innerText);
    }
    
    remove = (index)=>{
        this.setState((prevState)=>{
            const prevTodos = [...prevState.todos];
            prevTodos.splice(index,1);
            return {todos:prevTodos}
        })
    }
   
    render(){
        const todos = [...this.state.todos];
        return <>
        <button onClick={this.fetchTodos} >{this.state.todosTitle}</button>
        <TodosHeader ref={this.buttonRef}/>
        {
        this.state.show && todos.map((item, index)=>{return <ul key ={index} ><Todos index = {index} remove = {this.remove}  id = {item.id} userId = {item.userId} title = {item.title} /></ul>}) }
        </>
    }
}

export default TechBase;
const TodosHead = (props,ref)=>{
    return <h1 ref={ref}> I am todos header</h1>
}
const TodosHeader = React.forwardRef(TodosHead);
/*export class TodosHeader extends React.Component{
    render(){
        return <h1> I am todos header</h1>
    }
}*/


//export const TodosHeader = Todoshead;