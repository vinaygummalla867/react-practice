import React from 'react';

export default class DisplayLocation extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            location:''
            
        };
    }

    render(){
        return this.props.toggle ?  <div>
        <p>Location</p>
        <input type='text' value={this.state.location} onChange={(e)=>{this.setState({location:e.target.value})}}/>
    </div> : <p>{this.state.location} is the current location</p>
        
       
    }
}