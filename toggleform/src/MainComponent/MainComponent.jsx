import React from 'react';
import DisplayLogin from '../DisplayLogin/DisplayLogin';
import DisplayEmail from '../DisplayEmail/DisplayEmail';
import DisplayLocation from '../DisplayLocation/DisplayLocation';


export default class Main extends React.Component{

    constructor(props){
        super(props);
        this.state = {toggle:false}
    }
    render(){
        const divStyle = {margin:'auto',backgroundColor:'yellow',width:'30vw',marginTop:'3vh',padding:'4vw',borderRadius:'5%'};
        return <div style={divStyle}>
            <h2 style={{textAlign:'center',color:'red'}}>Form</h2>
            <input type="checkbox" onChange={()=>{this.setState({toggle:!this.state.toggle})}}/>
            <DisplayLogin toggle={this.state.toggle}/>
            <DisplayEmail toggle={this.state.toggle}/>
            <DisplayLocation toggle={this.state.toggle}/>
        </div>
    }
}