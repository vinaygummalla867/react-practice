import React from 'react';

export default class DisplayEmail extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email:''
            
        };
    }

    render(){
        return this.props.toggle ?  <div>
        <p>Email</p>
        <input type='email' value={this.state.email} onChange={(e)=>{this.setState({email:e.target.value})}}/>
    </div> : <p>{this.state.email} is the mail ID</p>
        
       
    }
}