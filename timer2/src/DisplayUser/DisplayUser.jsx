import React from "react";
import { connect } from "react-redux";
import { userNameChangeCreator } from "../Utils/actions";

class DisplayUser extends React.Component{

    constructor(props){
        super(props);
        this.state = {userName:'',
                      editMode:false   }
    }

    changeEditMode = ()=>{
      this.setState({editMode:!this.state.editMode});
    }

    
    userNameInput = (e)=>{
        this.setState({userName:e.target.value});
    }

    render(){
        const {userNameChangeRedux} = this.props;
        return <>
           
           {this.props.inputMode && <p>The Current User: {this.props.user}  <button onClick={this.changeEditMode}>Edit</button>
           <button onClick={()=>{this.setState({editMode:false})}}>Cancel</button></p>}
           { this.props.inputMode && this.state.editMode && <p> <input type = "text" value = {this.state.userName} 
           onChange = {this.userNameInput}  /> 
           <button onClick={()=>userNameChangeRedux(this.state.userName)}>Save</button>  </p>               }
        </>
    }

}

const mapStateToProps = (state)=>({
    user:state.user.userName
});

const mapDispatchToProps = (dispatch)=>({
    userNameChangeRedux:(userName)=>{dispatch(userNameChangeCreator(userName))}
});

const Hoc = connect(mapStateToProps, mapDispatchToProps);

export default Hoc(DisplayUser);