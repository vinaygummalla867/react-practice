import React from "react";

const colors = ["red", "yellow", "green", "blue", "orange", "brown", "violet" ];

class ChangeColor extends React.Component{

    constructor(props){
        super(props);
        this.state = {bgColor:''};
    }

    randomNumber = (min, max)=>{
        
            return Math.floor(Math.random() * (max - min) + min);
   
    }
   start = ()=>{
       this.intervalId = setInterval(()=>{
            let index = this.randomNumber(0,6);
           this.setState({bgColor:colors[index]});
       }, 1000)
   }
    stop = ()=>{
        clearInterval(this.intervalId);
    }
    render(){
        return <><button style = {{backgroundColor:this.state.bgColor}}>Color</button>
        <button onClick={this.start}>Start</button><button onClick={this.stop}>Stop</button>
        </>
    }
}

export default ChangeColor;