import React from 'react';
import DisplayTime from '../DisplayTime/DisplayTime';
import TimeData from '../TimeData/TimeData';
import DisplayUser from '../DisplayUser/DisplayUser';
import DisplayEmail from '../DisplayEmail/DisplayEmail';
import DisplayId from '../DisplayId/DisplayId';

class Main extends React.Component{
    constructor(props){
        super(props);
        this.state = {inputMode:false}
    }

    inputModeChange = ()=>{
        this.setState({inputMode:!this.state.inputMode})
    }
    render(){

        return <>
        
        <DisplayTime inputMode = {this.state.inputMode}/>
        <TimeData /> <br/>
        <input type = "checkbox" value = {this.state.inputMode} onChange = {this.inputModeChange} />
        <DisplayUser  inputMode = {this.state.inputMode}/>
        <DisplayEmail inputMode = {this.state.inputMode}/>
        <DisplayId  inputMode = {this.state.inputMode}/>
    </>
    }
} 
   


export default Main;