import React from "react";
import { connect } from "react-redux";
import { emailChangeAction } from "../Utils/actions";

class DisplayEmail extends React.Component{

    constructor(props){
        super(props);
        this.state = {email:'',
    editMode:false};
    }

    editModeChange = ()=>{
        this.setState({editMode:!this.state.editMode});
    }

    emailChange = (e)=>{
        this.setState({email:e.target.value});
    }
    render(){

        return <>
            {this.props.inputMode && <p>Current Email ID: {this.props.email}  <button onClick={this.editModeChange}>Edit</button>
            <button onClick={()=>{this.setState({editMode:false})}}>Cancel</button></p>}
            {this.props.inputMode && this.state.editMode && <p> <input type = "email" value = {this.state.email}
            onChange = {this.emailChange}/> <button onClick={()=>{this.props.emailToRedux(this.state.email)}}>Save</button></p>}
        </>
    }
}

const mapStateToProps = (state)=>(
    {email:state.user.email}
);
const mapDispatchToProps = (dispatch)=>(
    {emailToRedux:(email)=>{dispatch(emailChangeAction(email))}}
);


const Hoc = connect(mapStateToProps, mapDispatchToProps);

export default Hoc(DisplayEmail);