export const incrementAction = (times)=>({type:"Increment", payload:times});

export const decrementAction = (times)=>({type:"Decrement", payload:times});

export const userNameChangeCreator = (userName)=>({type:"userNameChange", payload:userName});

export const emailChangeAction = (email)=>({type:"emailChange", payload:email});

export const idChangeAction = (id)=>({type:"idChange", payload:id});


export const fetchTodos = ()=>{

    return (dispatch)=>{
        try{
            fetch("https://jsonplaceholder.typicode.com/todos").then(res=>res.json()).then(data=>{
            dispatch({type:"ADD_TODOS", payload:data})  //doubt
        
        })
        }
        catch(e){
            console.log(e);
        }
        
    }
}