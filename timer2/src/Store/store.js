import { createStore, applyMiddleware } from "redux";
import timerReducer from "./reducer";
import ThunkMiddleware from "redux-thunk";

const store = createStore(timerReducer, applyMiddleware(ThunkMiddleware));

export default store;