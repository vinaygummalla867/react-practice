import { combineReducers } from "redux";


const timerReducer = (state={timer:0}, action)=>{

    switch(action.type){

        case "Increment": return {...state, timer:state.timer + action.payload};
        case "Decrement": return {...state, timer:state.timer - action.payload};
        default: return state
    }

}
const userReducer = (state = {userName:'', email:'', userId:''}, action)=>{
    switch(action.type){
        case "userNameChange": return {...state, userName:action.payload};
        case "emailChange": return {...state, email:action.payload};
        case "idChange": return {...state, userId:action.payload };
        default: return state
    }
}

const todosReducer = (state=[], action)=>{
    switch(action.type){
        case "ADD_TODOS": return [...state, ...action.payload]
        default: return state
    }
}

const reduxReducer = combineReducers({user:userReducer, timer:timerReducer, todos:todosReducer})
export default reduxReducer;