import { connect } from "react-redux";

const DisplayTime = (props)=>{
    return <>
        <p>The Timer current value is:{props.timerValue}</p>
    </>
}

const mapStateToProps = (state)=>({
    timerValue:state.timer.timer
});

const Hoc = connect(mapStateToProps);

export default Hoc(DisplayTime);
