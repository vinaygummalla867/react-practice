import React from "react";
import { connect } from "react-redux";
import { incrementAction, decrementAction, fetchTodos } from "../Utils/actions";


class TimeData extends React.Component{
    constructor(props){
        super(props);
        this.state = {times:0};
      this.gbRef = React.createRef();
        
    }
    componentDidMount(){
        this.props.fetchTodos();
      //  console.log(this.gbRef.current.innerText);
    }
    componentDidUpdate(){
       //console.log(this.gbRef.current.innerText);
    }
    changeTimes = (e)=>{
        this.setState({times:Number(e.target.value)});
    }

    render(){
      
        return  <>
          <input type="number" value={this.state.times} onChange={this.changeTimes} />   
          <button onClick={()=>{this.props.incrementTimer(this.state.times)}}>+</button>
          <button onClick={()=>{this.props.decrementTimer(this.state.times)}}>-</button>
         <ul>
              {this.props.todos?.map((item, index)=>{
                  const liRef = React.createRef();
                  return <li key = {index} ref = {liRef}>{item.title}</li>}
          )}  </ul>  
          <RefComp ref={this.gbRef} >
              <h3>This is forwarded ref</h3>
              </RefComp> 
                </>
    }
}

const mapDispatchToProps = (dispatch)=>({
    incrementTimer:(times)=>{dispatch(incrementAction(times))},
    decrementTimer:(times)=>{dispatch(decrementAction(times))},
    fetchTodos:()=>{dispatch(fetchTodos())}
})

const mapStateToProps = (state)=>({
    todos:state.todos
});

const RefComp = React.forwardRef((props, ref)=>{
   // console.log(ref.current.innerText);
    return <h1 ref={ref} ></h1>
});

const Hoc = connect(mapStateToProps, mapDispatchToProps);

export default Hoc(TimeData);
