import React from "react";
import { connect } from "react-redux";
import { idChangeAction } from "../Utils/actions";

class DisplayId extends React.Component{

    constructor(props){
        super(props);
        this.state = {Id:'', editMode:false};
    }
    editModeChange = ()=>{
        this.setState({editMode:!this.state.editMode});
    }
    idInput = (e)=>{
        this.setState({Id:e.target.value});
    }
    render(){
        return <>
            {this.props.inputMode && <p>Curret user's Id: {this.props.Id}  <button onClick={this.editModeChange}>Edit</button>
            <button onClick={()=>{this.setState({editMode:false})}}>Cancel</button></p>}
            {this.props.inputMode && this.state.editMode &&  <p><input type = "text" value = {this.state.Id} 
            onChange = {this.idInput} /> <button onClick={()=>{this.props.idChange(this.state.Id)}} >Save</button> </p>  }
        </>
    }
}


const mapStateToProps = (state)=>(
    {Id:state.user.userId}
);

const mapDispatchToProps = (dispatch)=>(
    {idChange:(id)=>{dispatch(idChangeAction(id))}}
);

const Hoc = connect(mapStateToProps,mapDispatchToProps);

export default Hoc(DisplayId);