import React from 'react';
import HigherOrderComponent from '../HigherOrderComponent/HigherOrderComponent';

class DisplayEmail extends React.Component{
        constructor(props){
            super(props);
            this.state = {email:''}
        }

        render(){
            const {editMode,editModeChange,toggle} = this.props;
            return <>
          <p>{`Email`} {toggle && !editMode && <button onClick={editModeChange}>Edit</button>} </p>
          {toggle && editMode ? <> <input type="email" value={this.state.value} onChange={(e)=>{this.setState({email:e.target.value})}}/>
        <button onClick={editModeChange}>Save</button><button onClick={editModeChange}>Cancel</button></> : 
        <p>{this.state.email}</p>}</>
        }
}

export default HigherOrderComponent(DisplayEmail);