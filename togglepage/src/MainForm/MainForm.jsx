import React from 'react';
import DisplayUser from '../DisplayUsername/DisplayUsername';
import DisplayEmail from '../DisplayEmail/DisplayEmail';
import DisplayLocation from '../DisplayLocation/DisplayLocation';
import './MainForm.css';

export default class MainForm extends React.Component{

        constructor(props){
            super(props);
            this.state = {toggle:false};
        }


        render(){
            const divStyle = {margin:'auto', marginTop:'5vh', padding:'3vw', width:'30vw', backgroundColor:'Yellow', borderRadius:'4%'}
            return  <div style={divStyle}>
                <h2>Form</h2>
                <input type="checkbox" onChange={()=>{this.setState({toggle:!this.state.toggle})}}/>
              <DisplayUser toggle={this.state.toggle}/> 
              <DisplayEmail toggle={this.state.toggle}/> 
              <DisplayLocation toggle={this.state.toggle}/>    </div>

        }

}