import React from 'react';
import HigherOrderComponent from '../HigherOrderComponent/HigherOrderComponent';

class DisplayUser extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            username:''
        };
    }
    
    render(){
        const {toggle,editMode,editModeChange} = this.props;
        return <>
        <p>{`Username` }   {toggle && !editMode && <button onClick={editModeChange}>Edit</button>} </p> 
           {toggle && editMode ? <><input type="text" value={this.state.username} onChange={(e)=>{this.setState({username:e.target.value})}}/>
           <button onClick={editModeChange}>Save</button> <button onClick={editModeChange}>Cancel</button> </>
           : <p>{this.state.username}</p>}
        </>
    }
}

export default HigherOrderComponent(DisplayUser);