import React from 'react';
import HigherOrderComponent from '../HigherOrderComponent/HigherOrderComponent';

class DisplayLocation extends React.Component{
    constructor(props){
        super(props);
        this.state ={location:''};
    }

    render(){
        const {toggle, editMode, editModeChange} = this.props;
        return <>
        <p>Location {toggle && !editMode && <button onClick={editModeChange}>Edit</button>}</p>
        {toggle && editMode ? <><input type='text' value={this.state.value} onChange={(e)=>{this.setState({location:e.target.value})}}/>
        <button onClick={editModeChange}>Save</button><button onClick={editModeChange}>Cancel</button> </> : 
        <p>{this.state.location}</p>}
        
        </>
    }
}

export default HigherOrderComponent(DisplayLocation);