import React from 'react';

const HigherOrderComponent = (ParameterComponent)=>{

    class Hoc extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                  editMode:false  
            };
        }
    editModeChange = ()=>{
            this.setState({editMode:!this.state.editMode});
    }
    render(){
            return <ParameterComponent {...this.props} editMode={this.state.editMode} editModeChange={this.editModeChange}/>
            
           
        }
    }
    return Hoc
};

export default HigherOrderComponent;