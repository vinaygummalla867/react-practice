import React from 'react';
import Input from '../InputComponent/Input';
import './SignUp.css';
import {pwdPattern,emailPattern,fnPattern,lnPattern} from '../utils';
import UserDetails from '../UserComponent/UserDetails';

export default class SignUp extends React.Component{
    constructor(props){
        super(props);
        this.state={
                    };
    };

    
    
//lifting the state upwards


    render(){
       //const divStyle={width:'30%', height:'34%',margin:'auto',marginTop:'8vh',borderRadius:'.6rem', backgroundColor:'gray'};
       //const formStyle={padding:'1rem'}
        return <>
                <h1>Sign Up</h1>
                <div class='form-container'>
                    <form class='form-element' onSubmit={this.props.submitForm}>
                        <label forName='first-name' >Enter your First Name:</label>
                        <Input type='text' value={this.props.firstName} id='first-name' class='sign-up'  change={this.props.firstNameChange}/>
                        <span>{this.props.fnError ? 'Your First name can only contain letters':''}</span><br/>
                        <label forName='last-name'  >Enter Your Last Name:</label>
                        <Input type='text' id='last-name' value={this.props.lastName} class='sign-up'   change={this.props.lastNameChange}/>
                        <span>{this.props.lnError ? 'Your Last name can only contain letters':''}</span><br/>
                        <label forName='E-mail' >Enter your Email Id:</label>
                        <Input type='text' id='E-mail' value={this.props.email} change={this.props.emailChange} class='sign-up' />
                        <span>{this.props.emailError ? 'Your Email must contain @ and a domain':''}</span><br/>
                        <label forName='pwd' >Choose a Password:</label>
                        <Input type='password' id='pwd' value={this.props.password} change={this.props.passwordChange} class='sign-up' />
                        <span>{this.props.pwdError ? 'Your password must be at least 8 characters with at least one uppercase, lowercase and special character each':''}</span><br/>
                        <Input type='submit' value='submit'/>
                    </form>
                </div>
                <UserDetails firstName={this.props.firstName}/>
        
        </>
    }

}