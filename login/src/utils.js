export const emailPattern = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
export const pwdPattern = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
export const fnPattern = /^[A-Za-z]+$/;
export const lnPattern = /^[A-Za-z]+$/;