import React from 'react';
import Dashboard from '../Dashboard/dashboard';
import SignUp from '../SignUp page/SignUp';


export default class AppComponent extends React.Component{
    constructor(props){
        super(props);
        this.state={
            currentPage:'Sign Up',
            firstName:'',
            lastName:'',
            email:'',
            password:'',
            fnError:false,
            lnError:false,
            emailError:false,
            pwdError:false


        }

    }
    submitForm = (event)=>{
        event.preventDefault();
     //   this.setState({fnError:!fnPattern.test(this.state.firstName)});
       
      //   this.setState({lnError:!lnPattern.test(this.state.lastName)});
        
       
       //     this.setState({emailError:!emailPattern.test(this.state.email)});
        
        
        //    this.setState({pwdError:!pwdPattern.test(this.state.password)});

        
       /* if(this.state.firstName.length<5){
            this.setState({fnError:true});
            console.log(this.state.fnError);
        }
        if(this.state.lastName.length>11){
            this.setState({lnError:true});
            console.log(this.state.lnError);
        }
        if(this.state.password.length<8){
            this.setState({pwdError:true});
            console.log(this.state.pwdError);
        }
        if(this.state.email.length>15){
            this.setState({emailError:true});
               console.log(this.state.emailError);
        }
        if()
   */
        this.setState({currentPage:'dashboard'});
    }
    firstNameChange = (event)=>{
        this.setState({firstName:event.target.value});
    };
    lastNameChange =(event)=>{
        this.setState({lastName:event.target.value});
    };
    emailChange =(event)=>{
        this.setState({email:event.target.value});
    };
    passwordChange =(event)=>{
        this.setState({password:event.target.value});
    }
    navigatePage = ()=> this.setState({currentPage:this.state.currentPage === 'Sign Up'? 'Dashboard':'Sign Up'})
    render(){
        return <>
        <button onClick={this.navigatePage}>{this.state.currentPage === 'Sign Up'? 'Dashboard':'Sign Up'}</button>
        {this.state.currentPage === 'Sign Up' ? <SignUp 
            firstName={this.state.firstName}
            lastName={this.state.lastName}
            email={this.state.email}
            password={this.state.password}
            fnError={this.state.fnError}
            lnError = {this.state.lnError}
            emailError = {this.state.emailError}
            pwdError = {this.state.pwdError}
            firstNameChange = {this.firstNameChange}
            lastNameChange = {this.lastNameChange}
            emailChange = {this.emailChange}
            passwordChange = {this.passwordChange}
            submitForm = {this.submitForm}
        />:<Dashboard firstName = {this.state.firstName} lastName = {this.state.lastName}/>}
        </>;
    }
}