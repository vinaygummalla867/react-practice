import React from 'react';
import Button from '../changecolor/changebuttoncolor';
export default class Timer extends React.Component{
   constructor(){
       super();
       this.state ={
           timer:0,
           value:true

       }
   }
  timerIncrease = ()=>{
      this.setState((previousState)=>{
          return {timer:previousState.timer + 1}
      })
  }
  timerDecrease = ()=>{
    this.setState((previousState)=>{
        return {timer:this.state.timer === 0 ? 0:previousState.timer - 1 }
    })
}
  changeValue = ()=>{
      this.setState(()=>{
          return {value:!this.state.value}
      });
      console.log('Value Changed',this.state.value);
  }
   render(){
       return <> <button onClick={this.changeValue}>Change</button>
      
       {this.state.value &&  <Button/> }
       
     
      {this.state.value === false && <><button onClick={this.timerIncrease}>Increment</button>
       <button onClick={this.timerDecrease}>Decrement</button>
       <p>The timer value is {this.state.timer}</p></>} 
       
       </>
   }
}