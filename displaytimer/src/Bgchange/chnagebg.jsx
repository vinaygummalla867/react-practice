import React from "react";


let colors=['red','green','blue','yellow','purple','brown','orange'];
export default class ChangeBg extends React.Component{
    constructor(){
        super();
        this.state ={
            bgColor:''
        }
    }
    randomColor =(max,min)=>{
        return Math.floor(Math.random()*(max-min) + min);
        
    }

    componentDidMount(){
       this.intervalId = setInterval(()=>{
            let index = this.randomColor(0,6);
            this.setState(()=>{
                return {bgColor:colors[index]}
            })
        },1000);
        console.log('changeBg',this.intervalId);
        console.log('ChangeBg component did mount successfully');
    }

    componentDidUpdate(){
        console.log('ChangeBg component did update');
    }
    componentWillUnmount(){
        console.log(clearInterval(this.intervalId));
        console.log('ChangeBg component will be unmounting');
    }
    render(){
        return <div style={{textAlign:'center'}}><button  style={{backgroundColor:this.state.bgColor}}>BgChange</button></div>
    }



}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        