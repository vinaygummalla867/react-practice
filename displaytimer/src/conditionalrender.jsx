import React from "react";
import ChangeBg from "./Bgchange/chnagebg";
import ChangeTimer from "./Changetimer/changetimer";

export default class Conditional extends React.Component{
    constructor(){
        super();
        this.state={
            value:true
        }
    }
    componentDidMount(){
        console.log('Conditional render mounted');
        setInterval(()=>{
            this.setState(()=>{
                return {value:!this.state.value}
            });
        },20000);
    }

    render(){
        return <>{this.state.value && <ChangeBg/>}

        {this.state.value === false && <ChangeTimer/> }


</>
    }
}

