import React from 'react';
//import ChangeBg from '../Bgchange/chnagebg';


export default class ChangeTimer extends React.Component{
    constructor(){
        super();
        this.state={
            timerValue:0
        }
    }
    componentDidMount(){
        console.log('ChangeTimer Component mounted successfully');
       
    }
    componentDidUpdate(){
        console.log('ChangeTimer Component updated successfully');
    }
    componentWillUnmount(){
        console.log('ChangeTimer Component is going to be unmounted');
    }
    timerIncrement = ()=>{
        this.setState((previousState)=>{
            return {timerValue:previousState.timerValue+1}
        });
    }
    timerDecrement = ()=>{
        this.setState((previousState)=>{
            return {timerValue:previousState.timerValue === 0 ? 0 : previousState.timerValue-1}
        })
    }

    render(){
        return <> 
      
         <p>The timer value is {this.state.timerValue}</p>
        <button onClick={this.timerIncrement}>Increment</button>
        <button onClick={this.timerDecrement}>Decrement</button>
        </>
    }
}