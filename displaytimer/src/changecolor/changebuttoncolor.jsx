import React from 'react';
let colors=['green','red','yellow','blue','orange','purple','purple'];
export default class Button extends React.Component{
    constructor(){
        super();
        this.state={
            color:''
        }   
    }
    randomSelector = (min, max)=>{
        return Math.floor(Math.random() * (max - min) + min);
    }
    componentDidMount(){
        console.log('Component did Mount successfully');
        
        const intervalId = setInterval(()=>{
            let index = this.randomSelector(0,6);
            this.setState(()=>{
                return {color:colors[index]}
            });
        },1000);
    };
    componentDidUpdate(){
        console.log('Component did update successfully');
    }
    componentWillUnmount(){
        console.log('Component will unmount successfully');
    }
    render(){
        return <button style={{backgroundColor:this.state.color}}>COLORChange</button>
    }
}