import React from 'react';
import './studentlist.css';


export default class StudentList extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return <p> {this.props.fname} {this.props.lname} is {this.props.age} years old and is studying in {this.props.branch} section {this.props.section}</p>;
                
         
    }
}