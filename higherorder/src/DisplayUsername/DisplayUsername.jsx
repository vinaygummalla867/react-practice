import React from 'react';
import HigherOrderComponent from '../HigherOrderComponent/HigherOrderComponent';

class DisplayUsername extends React.Component{
    constructor(props){
        super(props);
        this.state = {
           
            username:''
        }

    }
    setUsername = (e)=>{
        this.setState({username:e.target.value});
        //this.props.toggleEditMode();
    }
    render(){
        const {editmode, toggle,toggleEditMode} = this.props;
        return <> 
        <p>{`User name `}{toggle && !editmode && <button onClick={toggleEditMode}>Edit</button>}</p>
         {toggle && editmode ? (<> 
         <input type='text' value={this.state.username} onChange={this.setUsername}/>
         <button onClick={toggleEditMode}>Save</button><button onClick={toggleEditMode}>Cancel</button>
         </>): <p>{this.state.username}</p>  
        }
        {this.props.children}
        
        </>
                
               
    }               
};

export default HigherOrderComponent(DisplayUsername);