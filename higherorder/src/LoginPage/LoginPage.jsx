import React from 'react';

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    submitForm = (e)=>{
        e.preventDefault();
        //localStorage.setItem("username",this.state.username);
        //console.log('before',this.state.username);
        this.props.submitForm(this.state.username);
        //console.log('after',this.state.username);
    }

    render(){
        return <div>
            <form onSubmit={(e)=>this.submitForm(e)}>
            <p>User Name</p>
            <input type="text" value={this.state.username} onChange={(e)=>{this.setState({username:e.target.value})}}/>
            <p>Password</p>
            <input type="password" value={this.state.password} onChange={(e)=>{this.setState({password:e.target.value})}}/>
            <br/>
            <input type="submit" value="Submit"/>
            </form>
        </div>
    }
}
export default Login;