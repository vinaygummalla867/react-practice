import React from 'react';
import HigherOrderComponent from '../HigherOrderComponent/HigherOrderComponent';

class DisplayLocation extends React.Component{
    constructor(props){
        super(props);
        this.state = {location:''};
    }

    render(){
        return <div> <p>Location</p>
        {this.props.toggle ? <input type="text" value={this.state.location} onChange={(e)=>{this.setState({location:e.target.value})}}/> :
        <p>{this.state.location}</p> }


        </div>
        
        
    }
}

export default HigherOrderComponent(DisplayLocation);