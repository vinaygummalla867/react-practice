import React from 'react';
import DisplayComp from '../DisplayPage/DisplayPage';
//import HigherOrderComponent from '../HigherComponent/HigherOrderComp';

class Main extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            products:[{brand: 'Adidas',price: 2300,type: 'Footwear',quantity: 0},
                      {brand: 'Nike',price: 3500,type: 'Footwear',quantity: 0},
                      {brand: 'UCB',price: 3000,type: 'Clothing',quantity: 0},
                      {brand: 'Safari',price: 1000000,type: 'SUV',quantity: 0},
                      {brand: 'Harrier',price: 1400000,type: 'SUV',quantity: 0},
                      {brand: 'Pepsi',price: 50,type: 'Beverage',quantity: 0},
                      {brand: 'ThumsUp',price: 55,type: 'Beverage',quantity: 0}]
        }
    }
    render(){
        const products = [...this.state.products];
        //console.log('Mainpage',products);
        return <DisplayComp products={products} />
    }
};

export default Main;