import React from 'react';
import LoginPage from '../LoginPage/LoginPage';


const HigherOrderComponent = (ParameterComponent)=>{

    class Hoc extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                quality:'best',
                username:''
            }
        }
        submitForm = (username)=>{
            this.setState({username});
            localStorage.setItem("username",username);
        }
        
        render(){
            //console.log( this.props); //{products:[],xyz:"abc"}
            //{products:[],xyz:"abc"}
            const username = localStorage.getItem("username");
            

            return username ?  <div>
            <p>{`Hello ${username}`}</p>
            <ParameterComponent {...this.props}/>
            <button onClick={()=>{localStorage.setItem("username",'');this.setState({username:''})}}>Logout</button>
        </div> : <LoginPage submitForm={this.submitForm}/> // products={this.props.products} xyz = {this.props.xyz}
        }
    }
    return Hoc
}

export default HigherOrderComponent;