import React from 'react';

const HigherOrder = (ParameterComponent)=>{
    class Hocc extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                        editmode:false
            };
        }
        toggleEditMode = ()=>{
            this.setState({editmode:!this.state.editmode});
        }
        render(){
            return <ParameterComponent {...this.props} editmode = {this.state.editmode} toggleEditMode ={this.toggleEditMode}/>
        }
    }
    return Hocc

}
export default HigherOrder;