import React from 'react';
import HigherOrderComponent from '../HigherOrderComponent/HigherOrderComponent';

class DisplayEmail extends React.Component{
    constructor(props){
        super(props);
        this.state = {email:''};
    }

    render(){
        return <div><p>Email</p>
        {this.props.toggle ? <input type="email" value={this.state.email} onChange={(e)=>{this.setState({email:e.target.value})}}/> :
        <p>{this.state.email}</p>  
    }
        </div>
    }
}

export default HigherOrderComponent(DisplayEmail);