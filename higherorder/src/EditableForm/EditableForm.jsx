import React from 'react';
import DisplayUsername from '../DisplayUsername/DisplayUsername';
import DisplayEmail from '../DisplayEmail/DisplayEmail';
import DisplayLocation from '../DisplayLocation/DisplayLocation';
import MainContainer from "../MainContainer/MainContainer";
export default class EditableForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            toggle:false
        }
       
    }
    toggleUser = ()=>{
        this.setState({toggle:!this.state.toggle});
    }
    render(){
        return <MainContainer>
                <input type="checkbox" value={this.state.toggle} onChange={this.toggleUser}/>
                <DisplayUsername toggle={this.state.toggle}>
                    <p>footer</p>
                </DisplayUsername>
                        <DisplayEmail toggle={this.state.toggle}/><br/>
                    <DisplayLocation toggle={this.state.toggle}/>
        
        </MainContainer>
    }

  
}