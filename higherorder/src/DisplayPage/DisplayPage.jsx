import React from "react";
import HigherOrderComponent from "../HigherComponent/HigherOrderComp";

class DisplayComp extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
       // console.log('DisplayPage',this.props.products);
    
        return this.props.products.map((item,index)=>{
            return<p  key={index}> {item.brand} costs {item.price} and can be found in the {item.type} section of the store</p>
        })
    }
}

export default HigherOrderComponent(DisplayComp);