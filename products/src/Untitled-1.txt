

let obj1 = {a:1,b:2,c:3};


let {a,b,c} = obj1;

-->let a = obj1.a; 
    let b = obj1.b;
    let c = obj1.c; ----->{a,b,c} = obj1;


    let x = obj1.a;
    let y = obj1.b;
    let z = obj1.c; ---->{a:x,b:y,c:z} = obj1;