import React from 'react';

/*export default class Display extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){

        const {products,removeProduct,editProduct} = this.props;
        return <>
        <h2 style={{textAlign:'center', color:'red'}}> Products</h2>
        <div style={{width:'35vw',margin:'auto',paddingLeft:'10px'}}>
        {products.map((item,index)=>{
            return <p key= {index} style={{color:'blue', marginTop:'5px'}}><span style={{color:'green',fontWeight:'bolder'}}>{item.brand}</span> costs {item.price} and found in the {item.type} showroom. <button onClick={()=>{editProduct(index)}}>Edit</button>
            <button onClick={()=>{removeProduct(index)}}>Remove</button></p>
            
        })}
        </div></>
    }
}  */


/*export default function Display(props){
    const {products,removeProduct,editProduct,...remainingProps} = props;
    return <>
    <h2 style={{textAlign:'center', color:'red'}}> Products</h2>
    <div style={{width:'35vw',margin:'auto',paddingLeft:'10px'}}>
    {products.map((item,index)=>{
        return <p key= {index} style={{color:'blue', marginTop:'5px'}}><span style={{color:'green',fontWeight:'bolder'}}>{item.brand}</span> costs {item.price} and found in the {item.type} showroom. <button onClick={()=>{editProduct(index)}}>Edit</button>
        <button onClick={()=>{removeProduct(index)}}>Remove</button></p>
        
    })}
    </div></>
}
*/



export default function Display({products,removeProduct,editProduct,...remainingProps}){
    //const {products,removeProduct,editProduct,...remainingProps} = props;
    console.log(remainingProps);
    return <>
    <h2 style={{textAlign:'center', color:'red'}}> Products</h2>
    <div style={{width:'35vw',margin:'auto',paddingLeft:'10px'}}>
    {products.map((item,index)=>{
        return <p key= {index} style={{color:'blue', marginTop:'5px'}}><span style={{color:'green',fontWeight:'bolder'}}>{item.brand}</span> costs {item.price} and found in the {item.type} showroom. <button onClick={()=>{editProduct(index)}}>Edit</button>
        <button onClick={()=>{removeProduct(index)}}>Remove</button></p>
        
    })}
    </div></>
}