import React from 'react';
import Display from '../Display/displayproducts';

export default class Products extends React.Component{
    constructor(props){
        super(props);
        this.state = {products:[
            {brand:'Nike',type:'Shoes',price:3000},
            {brand:'Sony',type:'TV',price:50000},
            {brand:'Samsung',type:'Mobiles',price:25000},
            {brand:'UCB',type:'Clothing',price:2500},
            {brand:'Safari',type:'SUV',price:2500000}
        ]};
    }
    editProduct = (index)=>{
            
            this.setState((previousState)=>{
                const products = [...previousState.products];
                products[index].price +=300;
                return {products:products}
            });
    }
    removeProduct = (index)=>{
            this.setState((previousState)=>{
                const products=[...previousState.products];
                products.splice(index,1);
                return {products}
            });
    }
    render(){
        const props = {products: this.state.products, editProduct: this.editProduct, removeProduct: this.removeProduct};

        return <Display {...props}/>
        /*
        <Display products={this.state.products} editProduct={this.editProduct} removeProduct={this.removeProduct}/>
        
        
        */
    }
}