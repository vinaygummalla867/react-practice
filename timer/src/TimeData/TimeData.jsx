import React from 'react';
import { incrementAction, decrementAction, fetchTodos} from '../store/actions';

import { connect } from 'react-redux';
export const BASE_URL = "https://jsonplaceholder.typicode.com/";
export const LIST_OF_TODOS_URL = `${BASE_URL}todos`;
export const USER_DETAIL_URL = `${BASE_URL}users`;  //doubts

class TimeData extends React.Component{

    constructor(props){
        super(props);
        this.state = {input:0};
        this.liRef = React.createRef();
    }
    componentDidMount(){
        this.props.fetchTodos();
       // console.log("mounting:", this.liRef.current.innerText);// Error? Why? Cos fetchTodos is async?
       
    }
    componentDidUpdate(){
        console.log("updating:", this.liRef.current.innerText);
    }

     inputData = (e)=>{this.setState({input:Number(e.target.value)})};
    render(){
        
        return <>
            <input type="number" value ={this.state.input} onChange={this.inputData} />
            <button onClick={()=>this.props.incrementTime(this.state.input)} >+</button>
            <button onClick={()=>this.props.decrementTime(this.state.input)} >-</button>
            <ul>{
                this.props.todos?.map((todoItem)=><li ref = {this.liRef }>{todoItem.title}</li>)
                }</ul>
        </>
    }
}

const mapStateToProps = (state)=>({todos:state.todos});


const mapDispatchToProps = (dispatch)=>({
    incrementTime:(time)=>{dispatch(incrementAction(time))},
    decrementTime:(time)=>{dispatch(decrementAction(time))},
    fetchTodos:()=>{dispatch(fetchTodos())}
    
});

const Hoc = connect(mapStateToProps,mapDispatchToProps);

export default Hoc(TimeData);