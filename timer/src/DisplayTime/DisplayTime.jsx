import React from 'react';
import { connect } from 'react-redux';

 const DisplayTime = (props)=>{
    return <p>The time is {props.componentTime}</p>
};

const mapStateToProps = (state)=>({
    componentTime:state.incDec.time  //doubt
});

const Hoc = connect(mapStateToProps);
export default Hoc(DisplayTime);