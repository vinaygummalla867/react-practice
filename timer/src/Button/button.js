import React from "react";

const colors=['green','black','red','yellow','orange','green','purple'];
export default class LifeCycle extends React.Component {
    constructor(){
        super();
        this.state ={
            bgColor : 'green',
            intervalId:''
        };
    }
    componentDidMount(){
        console.log('Component mounted properly');
       // setInterval(()=>{
        //const index = this.randomNumber(0,6);
        //this.setState({bgColor:colors[index]});},1000);
    }
    randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    componentDidUpdate(){
        console.log('Component did update properly'); 
    }
    componentWillUnmount(){
        console.log('Component will unmount'); 
    }
    start=()=>{
        this.intervalId = setInterval(()=>{
        const index = this.randomNumber(0,6);
        this.setState({bgColor:colors[index]});},1000);
        



    }
    stop =()=>{
        clearInterval(this.intervalId);
    }
    render(){
        return <div style={{textAlign:'center'}}>
             <button style={{backgroundColor:this.state.bgColor}} type='button'>Button</button>
             <button onClick={this.start}>Start</button>
             <button onClick={this.stop}>Stop</button>
        </div>
    }

};