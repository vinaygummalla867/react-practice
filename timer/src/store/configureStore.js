import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducer';
import  ThunkMiddleware from 'redux-thunk';

const store = createStore(rootReducer,applyMiddleware(ThunkMiddleware));

export default store;


