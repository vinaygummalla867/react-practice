import { combineReducers } from "redux";

const incDecReducer = (state={time:0},action)=>{
    switch(action.type){
        case "INCREMENT": return {...state,time:state.time+action.payload};
        case "DECREMENT": return {...state,time:state.time-action.payload};
        default:return state;
    }

}
const userDetailsReducer = (state={name:'',email:'',id:''},action) => { //{'type':'',payload:{name:'',}}
    switch(action.type){
        case "LOGIN": return {...state,...action.payload};
        case "LOGOUT": return {...state,name:'',email:'',id:''};
        default:return state;
    }
}
const todosReducer = (state=[],action)=>{
    switch(action.type){
        case "ADD_TODOS": return [...state,...action.payload];        
        default:return state;
    }
} 
 const rootReducer = combineReducers({incDec:incDecReducer,user:userDetailsReducer,todos:todosReducer});
 export default rootReducer;